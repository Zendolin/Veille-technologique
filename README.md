# Veille accessibilité
Voici une liste de ressources autour de l'accessibilité.

## Web
### Outils
 - [Outils pour l'accessibilité](http://www.accessify.com/tools-and-wizards/)
 - [Validator W3C](https://validator.w3.org/)
 - [WAVE](http://wave.webaim.org/)

### HTML5
 - [HTML5 Accessibility](http://www.html5accessibility.com/)

### Référentiels
 - [Using Aria (mai 2017)](https://www.w3.org/TR/using-aria/)
 - [WAI](https://www.w3.org/WAI/)
 - [WCAG 2.0](https://www.w3.org/Translations/WCAG20-fr/)

### Autre
 - [10 raisons expliquant pourquoi les clients n'ont que faire l'accessibilité](http://www.pompage.net/traduction/10-raisons)
 - [Accessibilité - Les normes](https://www.technologie-handicap-accessibilite.net/dossiers/accessibilite-web/accessibilite-web-les-normes/)
 - [Accessibilité numérique](http://references.modernisation.gouv.fr/accessibilite-numerique)
 - [Agenda adapté dans OneNote](https://www.cartablefantastique.fr/outils-pour-compenser/adapter-lordinateur/agenda-numerique/)
 - [Ally Québec](http://a11yqc.org/)
 - [Comment l'accessibilité d'un site Web peut améliorer son référencement ?](http://www.journaldunet.com/solutions/0604/060428-referencement-accessibilite-sites-web.shtml)
 - [Comment rendre votre site accessible à tout le monde ?](https://www.lafabriquedunet.fr/creation-site-vitrine/articles/guide-accessibilite-site-web/)
 - [Développer accessible pour le web (Alex Bernier)](http://www.braillenet.org/20160125/slides/)
 - [Euracert](http://www.euracert.org/fr/)
 - [Faire un site web accessible](https://openclassrooms.com/courses/faire-un-site-web-accessible)
 - [Introduction à l'accessibilité](https://openweb.eu.org/articles/intro_accessibilite/)
 - [Introduction au RGAA](http://references.modernisation.gouv.fr/rgaa/introduction-RGAA.html)
 - [L'accessibilité, c'est bon pour la planète](https://openweb.eu.org/articles/l-accessibilite-c-est-bon-pour-la-planete)
 - [Lettre ouverte à BabouX](http://pouipouilabs.net/articles/lettre-ouverte-a-baboux)
 - [Liste des sites ayant obtenu le label AccessiWeb](http://www.accessiweb.org/index.php/galerie.html)
 - [Notes de formation d'Elisabeth Piotelat (25/01/2016)](https://perso.limsi.fr/wiki/doku.php/zabeth/accessiweb)
 - [Notes techniques (RGAA)](http://references.modernisation.gouv.fr/rgaa/notes-techniques.html)
 - [OpenWeb](https://openweb.eu.org)
 - [Retour d'expérience d'une utilisatrice malvoyante](http://april.org/retour-d-experience-d-une-utilisatrice-malvoyante)
 - [Steve Faulkner (Paris-Web 2013)](https://www.paris-web.fr/2013/conferences/html5-accessibility.php)
 - [Traduction - Mythes et idées fausses sur l'accessibilité](http://pouipouilabs.net/articles/traduction-mythes-et-idees-fausses-sur-laccessibilite)
 - [Usability - Material Design](https://material.io/guidelines/usability/accessibility.html#accessibility-style)
